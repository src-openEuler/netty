%global debug_package %{nil}

Name:             netty
Version:          4.1.114
Release:          2
Summary:          An asynchronous event-driven network application framework and tools for Java
License:          Apache-2.0
URL:              https://netty.io/
Source0:          https://github.com/netty/netty/archive/netty-%{version}.Final.tar.gz
Source1:          codegen.bash
Source2:          https://repo1.maven.org/maven2/io/netty/netty-jni-util/0.0.9.Final/netty-jni-util-0.0.9.Final-sources.jar
Patch0000:        0001-Remove-optional-dep-Blockhound.patch
Patch0001:        0002-Remove-optional-dep-conscrypt.patch
Patch0002:        0003-Remove-optional-deps-jetty-alpn-and-npn.patch
Patch0003:        0004-Remove-optional-dep-tcnative.patch
Patch0004:        0005-Disable-Brotli-and-ZStd-compression.patch
Patch0005:        0006-Do-not-use-the-Graal-annotations.patch
Patch0006:        0007-Do-not-use-the-Jetbrains-annotations.patch
Patch0007:        no-werror.patch
Patch0008:        reproducible.patch
Patch0009:        fix-strip.patch
Patch0010:        CVE-2025-24970.patch

BuildRequires:    autoconf automake libtool gcc
BuildRequires:    maven-local
BuildRequires:    mvn(com.fasterxml:aalto-xml)
BuildRequires:    mvn(com.jcraft:jzlib)
BuildRequires:    mvn(commons-logging:commons-logging)
BuildRequires:    mvn(io.netty:netty-tcnative-classes) >= 2.0.60
BuildRequires:    mvn(kr.motd.maven:os-maven-plugin)
BuildRequires:    mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:    mvn(org.apache.logging.log4j:log4j-1.2-api)
BuildRequires:    mvn(org.apache.logging.log4j:log4j-api)
BuildRequires:    mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:    mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:    mvn(org.apache.maven.plugins:maven-remote-resources-plugin)
BuildRequires:    mvn(org.bouncycastle:bcpkix-jdk15on)
BuildRequires:    mvn(org.bouncycastle:bctls-jdk15on)
BuildRequires:    mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:    mvn(org.codehaus.mojo:exec-maven-plugin)
BuildRequires:    mvn(org.fusesource.hawtjni:hawtjni-maven-plugin)
BuildRequires:    mvn(org.jctools:jctools-core)
BuildRequires:    mvn(org.slf4j:slf4j-api)

%description
Netty is an asynchronous event-driven network application framework
for rapid development of maintainable high performance protocol servers & clients.
%package    help
Summary:          Documents for %{name}
Buildarch:        noarch
Requires:         man info
Provides:         %{name}-javadoc = %{version}-%{release}
Obsoletes:        %{name}-javadoc < %{version}-%{release}
%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -p1 -n netty-netty-%{version}.Final

%pom_disable_module transport-rxtx
%pom_remove_dep -r :netty-transport-rxtx
%pom_disable_module transport-udt
%pom_remove_dep -r :netty-transport-udt
%pom_remove_dep -r :netty-tcnative-classes

%pom_remove_parent . bom dev-tools

%pom_disable_module "example"
%pom_disable_module "microbench"

%pom_xpath_inject 'pom:plugin[pom:artifactId="maven-remote-resources-plugin"]' '
<dependencies>
<dependency>
<groupId>io.netty</groupId>
<artifactId>netty-dev-tools</artifactId>
<version>${project.version}</version>
</dependency>
</dependencies>'

%pom_remove_plugin :maven-antrun-plugin
%pom_remove_plugin :maven-dependency-plugin
%pom_remove_plugin :xml-maven-plugin
%pom_remove_plugin -r :maven-checkstyle-plugin
%pom_remove_plugin -r :animal-sniffer-maven-plugin
%pom_remove_plugin -r :maven-enforcer-plugin
%pom_remove_plugin -r :maven-shade-plugin
%pom_remove_plugin -r :maven-release-plugin
%pom_remove_plugin -r :maven-clean-plugin
%pom_remove_plugin -r :maven-source-plugin
%pom_remove_plugin -r :maven-deploy-plugin
%pom_remove_plugin -r :maven-jxr-plugin
%pom_remove_plugin -r :maven-javadoc-plugin
%pom_remove_plugin -r :forbiddenapis
%pom_remove_plugin -r :revapi-maven-plugin
%pom_remove_plugin -r :bom-helper-maven-plugin
%pom_remove_plugin :japicmp-maven-plugin
%pom_remove_plugin :duplicate-finder-maven-plugin all
%pom_remove_plugin :flatten-maven-plugin all

cp %{SOURCE1} common/codegen.bash
chmod a+x common/codegen.bash
%pom_add_plugin org.codehaus.mojo:exec-maven-plugin common '
<executions>
    <execution>
        <id>generate-collections</id>
        <phase>generate-sources</phase>
        <goals>
            <goal>exec</goal>
        </goals>
        <configuration>
            <executable>common/codegen.bash</executable>
        </configuration>
    </execution>
</executions>
'
%pom_remove_plugin :groovy-maven-plugin common

%pom_remove_dep "org.graalvm.nativeimage:" common
rm common/src/main/java/io/netty/util/internal/svm/*

%pom_remove_dep -r "com.google.protobuf:protobuf-java"
%pom_remove_dep -r "com.google.protobuf.nano:protobuf-javanano"
rm codec/src/main/java/io/netty/handler/codec/protobuf/*
sed -i '/import.*protobuf/d' codec/src/main/java/io/netty/handler/codec/DatagramPacket*.java

%pom_remove_dep -r "org.jboss.marshalling:jboss-marshalling"
rm codec/src/main/java/io/netty/handler/codec/marshalling/*

%pom_remove_dep -r com.github.jponge:lzma-java
rm codec/src/*/java/io/netty/handler/codec/compression/Lzma*.java
%pom_remove_dep -r com.ning:compress-lzf
rm codec/src/*/java/io/netty/handler/codec/compression/Lzf*.java
%pom_remove_dep -r net.jpountz.lz4:lz4
rm codec/src/*/java/io/netty/handler/codec/compression/Lz4*.java
%pom_remove_dep -r com.aayushatharva.brotli4j:
rm codec/src/*/java/io/netty/handler/codec/compression/Brotli*.java
%pom_remove_dep -r com.github.luben:zstd-jni
rm codec/src/*/java/io/netty/handler/codec/compression/Zstd*.java

%pom_disable_module resolver-dns-native-macos
%pom_remove_dep -r :netty-resolver-dns-native-macos

%pom_disable_module testsuite
%pom_disable_module testsuite-autobahn
%pom_disable_module testsuite-http2
%pom_disable_module testsuite-native
%pom_disable_module testsuite-native-image
%pom_disable_module testsuite-native-image-client
%pom_disable_module testsuite-native-image-client-runtime-init
%pom_disable_module testsuite-osgi
%pom_disable_module testsuite-shading
%pom_disable_module transport-native-unix-common-tests

%pom_remove_dep io.netty:netty-jni-util transport-native-unix-common
%pom_remove_plugin :maven-dependency-plugin transport-native-unix-common
mkdir -p transport-native-unix-common/target/netty-jni-util
unzip %{SOURCE2} -d transport-native-unix-common/target/netty-jni-util

%pom_xpath_remove "pom:build/pom:plugins/pom:plugin[pom:artifactId = 'maven-bundle-plugin']/pom:executions/pom:execution[pom:id = 'generate-manifest']/pom:configuration/pom:instructions/pom:Import-Package" common/pom.xml

%pom_remove_dep -r :annotations-java5

%mvn_package ":::linux*:"

%mvn_package ':*-tests' __noinstall

%build
export CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS"
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt NOTICE.txt

%files help -f .mfiles-javadoc

%changelog
* Tue Feb 11 2025 wangkai <13474090681@163.com> - 4.1.114-2
- Fix CVE-2025-24970

* Thu Nov 14 2024 yaoxin <yao_xin001@hoperun.com> - 4.1.114-1
- Update to 4.1.114
  * Validate HTTP Method (#14280)
  * Release AdaptiveByteBuf when ownership could not be transfered. (#14285)
  * Make arenas reuse their last chunk more aggressively (#14291)
  * Only add Magazine to Set if we can ensure its removed again (#14292)
  * Ensure Chunk will not leak if init of AdaptiveByteBuf fails for whatever reason (#14295)
  * Correctly release one-off allocated chunks (#14302)
  * Ensure pooled memory is released when AdaptivePoolingAllocator is GC'ed (#14304)
  * Slices / duplicates of AdaptiveByteBuf must not escape the rootParent (#14310)
  * Fix sizeBucket bug in AdaptivePoolingAllocator (#14311)
  * AdaptiveByteBufAllocator: More strict reference counting for chunks (#14322)
  * Ensure we not store the DnsQueryContext for later removal when we couldnt obtain a query id (#14326)
  * Reduce memory fragmentation (#14333)
  * Properly free magazine chunks and avoid orphaned magazines (#14334)
  * Magazines must be freed under the expand lock (#14336)
  * Release message before failing promise when multiple requests are written while upgrade is in progress. (#14342)
  * Allow to reuse more then one session per host / port mapping (#14356)
  * Ensure writes will not fail when triggered after receiving UpgradeEvent.UPGRADE_SUCCESSFUL (#14362)
  * Refactor DnsNameResolver to be able to use different strategies when it comes to creating Channels for queries. (#14374)
  * DnsNameResolver: allow users to skip bind() during bootstrap (#14375)
  * DnsResolverBuilder methods should make it clear that these are for DatagramChannel (#14379)

* Tue Nov 12 2024 yaoxin <yao_xin001@hoperun.com> - 4.1.13-22
- Fix CVE-2024-29025

* Wed Dec 13 2023 yaoxin <yao_xin001@hoperun.com> - 4.1.13-21
- Fix CVE-2022-41881

* Sun Mar 05 2023 wulei <wulei80@h-partners.com> - 4.1.13-20
- Add strip

* Wed Nov 9 2022 liyanan <liyanan32@h-partners.com>  - 4.1.13-19
- Change source

* Mon Mar 07 2022 wangkai <wangkai385@huawei.com> - 4.1.13-18
- Remove lz4-java dependency

* Fri Dec 24 2021 wangkai <wangkai385@huawei.com> - 4.1.13-17
- This package depends on log4j.After the log4j vulnerability CVE-2021-45105 is fixed,the version needs to be rebuild.

* Fri Dec 17 2021 yaoxin <yaoxin30@huawei.com> - 4.1.13-16
- Fix CVE-2021-43797

* Wed Oct 27 2021 wangkai <wangkai385@huawei.com> - 4.1.13-15
- fix CVE-2021-37136 CVE-2021-37137

* Mon Aug 16 2021 wangyue <wangyue92@qq.com> - 4.1.13-14
- fix build error

* Sat Aug 14 2021 wangyue <wangyue92@qq.com> - 4.1.13-13
- fix build error

* Tue Apr 06 2021 wangxiao <wangxiao65@huawei.com> - 4.1.13-12
- Fix CVE-2021-21295 CVE-2021-21409

* Tue Mar 09 2021 wangyue <wangyue92@huawei.com> - 4.1.13-11
- fix CVE-2021-21290

* Thu Jan 28 2021 maminjie <maminjie1@huawei.com> - 4.1.13-10
- remove the BuildRequires lzma-java that is deprecated

* Fri Dec 04 2020 caodongxia <caodongxia@huawei.com> - 4.1.13-9
- fix CVE-2019-16869 CVE-2019-20444 CVE-2019-20445 CVE-2020-11612
 
* Wed Aug 26 2020 yaokai <yaokai13@huawei.com> - 4.1.13-8
 - Disable support for protobuf in the codecs module

* Mon Dec 23 2019 Shuaishuai Song <songshuaishuai2@huawei.com> - 4.1.13-7
- package init
